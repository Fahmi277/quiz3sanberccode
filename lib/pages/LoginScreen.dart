// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:quiz3/pages/HomeScreen.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController controllerUsername = TextEditingController();
  TextEditingController controllerPassword = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          TextFormField(
            controller: controllerUsername,
            decoration: InputDecoration(
                labelText: "Username",
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5)))),
          ),
          SizedBox(
            height: 20,
          ),
          TextFormField(
            controller: controllerPassword,
            decoration: InputDecoration(
                labelText: "Password",
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5)))),
          ),
          SizedBox(
            height: 20,
          ),
          SizedBox(
              width: double.infinity,
              // height: 75,
              child: ElevatedButton(
                  onPressed: () {
                    print(controllerUsername.text);
                    print(controllerPassword.text);

                    String username = controllerUsername.text;
                    String password = controllerPassword.text;

                    if (username == '' || password == '') {
                      const snackBar = SnackBar(
                        content: Text('Username dan Password harus diisi'),
                      );

                      ScaffoldMessenger.of(context).showSnackBar(snackBar);
                    }

                    if (password == "Sanbercode123") {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => HomeScreen(
                            userName: username,
                          ),
                          // Pass the arguments as part of the RouteSettings. The
                          // DetailScreen reads the arguments from these settings.
                        ),
                      );
                      // const snackBar = SnackBar(
                      //   content: Text('Password sesuai'),
                      // );

                      // ScaffoldMessenger.of(context).showSnackBar(snackBar);
                    } else {
                      const snackBar = SnackBar(
                        content: Text('Password Salah'),
                      );

                      ScaffoldMessenger.of(context).showSnackBar(snackBar);
                    }
                  },
                  child: Text("Login")))
        ],
      ),
    );
  }
}
